﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
    public partial class Form1 : Form
    {
        private Tv minhaTV;

        public Form1()
        {
            InitializeComponent();
            minhaTV = new Tv();

            LabelStatus.Text = minhaTV.Mensagem;
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (!minhaTV.GetEstado())
            {
                minhaTV.LigaTv();
                LabelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "OFF";
                // Canal
                ButtonAumentaCanal.Enabled = true; //Para ativar o botão porque por defeito está desativado
                ButtonDiminuiCanal.Enabled = true; //Para ativar o botão porque por defeito está desativado
                LabelCanal.Text = minhaTV.Canal.ToString(); //GetCanal é um int tem que ser convertigo para String
                // Volume
                ButtonAumentaVolume.Enabled = true;
                ButtonDiminuiVolume.Enabled = true;
                LabelVolume.Text = minhaTV.Volume.ToString();
                // Volume1
                TrackBarVolume1.Enabled = true;
                LabelVolume1.Text = minhaTV.Volume.ToString();
                TrackBarVolume1.Value = minhaTV.Volume;
            }
            else
            {
                minhaTV.DesligaTv();
                LabelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "ON";
                // Canal
                ButtonAumentaCanal.Enabled = true; //Para ativar o botão porque por defeito está desativado
                ButtonDiminuiCanal.Enabled = true; //Para ativar o botão porque por defeito está desativado
                LabelCanal.Text = "- -";
                // Volume
                ButtonAumentaVolume.Enabled = false;
                ButtonDiminuiVolume.Enabled = false;
                LabelVolume.Text = "- -";
                // Volume1
                TrackBarVolume1.Enabled = false;
                LabelVolume1.Text = "- -";
            }
            

        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {

                minhaTV.Canal = minhaTV.Canal + 1;
                LabelCanal.Text = minhaTV.Canal.ToString();

        }

        private void ButtonDiminuiCanal_Click(object sender, EventArgs e)
        {

                minhaTV.Canal = minhaTV.Canal - 1;
                LabelCanal.Text = minhaTV.Canal.ToString();
        }

        private void ButtonDiminuiVolume_Click(object sender, EventArgs e)
        {
            if (minhaTV.Volume > 0)
            {
                minhaTV.DiminuiVolume(1);
                LabelVolume.Text = minhaTV.Volume.ToString();
            }
        }

        private void ButtonAumentaVolume_Click(object sender, EventArgs e)
        {
            if (minhaTV.Volume < 100)
            {
                minhaTV.AumentaVolume(1);
                LabelVolume.Text = minhaTV.Volume.ToString();
            }
        }

        private void TrackBarVolume1_Scroll(object sender, EventArgs e)
        {
            minhaTV.Volume = TrackBarVolume1.Value;
            LabelVolume1.Text = minhaTV.Volume.ToString();

        }
    }
}
