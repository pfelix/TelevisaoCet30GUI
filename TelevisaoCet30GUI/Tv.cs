﻿using System;
using System.IO;

namespace TelevisaoCet30GUI
{
    public class Tv
    {
        #region Atributos

        private bool estado; //Tv está ligada ao desligada
        private int canal; //Para os canais canal
        private readonly string mensagem; //Para mandar mensagens metemos readonly para saber que é só get
        private int volume; //Para usar o volume - Mantemos o atributo do volume, mas passamos a utilizar a propriedade o nome é igual mas com a primeira letra maiúscula

        #endregion

        #region Propriedades

        public int Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        // é a mesma coisa que
        // public int Volume { get; set; }
        // O nome da propriedade é igual ao atributo mas com a letra maiúscula
        // Neste caso o atributo pode ser eleminada

        public int Canal
        {
            get { return canal; }
            set
            {
                if (value > 0 && value <= 20)
                {
                    canal = value;
                }
            }
        }

        public string Mensagem { get; private set; } // metemos privado no set porque não queremos utilizar fora da class



        #endregion

        #region Métodos - Construtores

        // Default
        public Tv ()
        {
            estado = false;
            Canal = 1;
            Volume = 50;
            Mensagem = "Nova tv criada com sucesso!";
        }

        #endregion

        #region Métodos - Outros

        //Gets e Sets
        //public int GetCanal () //GetCanal para obter o canal
        //{
        //    return canal;
        //}
        //public void MudaCanal (int canal) //SetCanal pata mudar o canal
        //{
        //    this.canal = canal;
        //}
        //public string EnviaMensagem() //GetMensagem
        //{
        //    return mensagem;
        //}
        // Foi eleminado porque foi criado uma propredade
        //public int GetVolume() //Get Volume
        //{
        //    return volume;
        //}
        //public void SetVolume (int volume)
        //{
        //    this.volume = volume;
        //}

        public void AumentaVolume (int valor) //Set Volume para aumentar volume
        {
            Volume += valor;
        }
        public void DiminuiVolume(int valor) //Set Volume para diminuir volume
        {
            Volume -= valor;
        }
        public bool GetEstado() //GetEstado para indicar se Tv está ligada ou desligada
        {
            return estado;
        }
        public void LigaTv()
        {
            if (!estado)
            {
                estado = true;
                LerInfo();
                Mensagem = "Tv Ligada!";
            }
        }
        public void DesligaTv()
        {
            if (estado)
            {
                estado = false;
                GravarInfo();
                Mensagem = "Tv Desligada!";
            }
        }

        private void GravarInfo()
        {
            string ficheiro = @"tvInfo.txt";

            string linha = Canal + ";" + Volume;

            StreamWriter sw = new StreamWriter(ficheiro, false); // o false é para quando está a guardar o ficheiro apagar o que está no ficheiro e escrever de novo

            if (!File.Exists(ficheiro))
            {
                sw = File.CreateText(ficheiro);
            }
            sw.WriteLine(linha);
            sw.Close();
        }

        private void LerInfo()
        {
            string ficheiro = @"tvInfo.txt"; 

            StreamReader sr;

            if (File.Exists(ficheiro))
            {
                sr = File.OpenText(ficheiro);

                string linha = "";

                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = new string[2];

                    campos = linha.Split(';');

                    Canal = Convert.ToInt32(campos[0]);
                    Volume = Convert.ToInt32(campos[1]);
                }
                sr.Close();
            }
        }


        #endregion



    }
}
