﻿namespace TelevisaoCet30GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ButtonOnOff = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelCanal = new System.Windows.Forms.Label();
            this.ButtonAumentaCanal = new System.Windows.Forms.Button();
            this.ButtonDiminuiCanal = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LabelVolume = new System.Windows.Forms.Label();
            this.ButtonAumentaVolume = new System.Windows.Forms.Button();
            this.ButtonDiminuiVolume = new System.Windows.Forms.Button();
            this.TrackBarVolume1 = new System.Windows.Forms.TrackBar();
            this.LabelVolume1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(11, 319);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(97, 16);
            this.LabelStatus.TabIndex = 0;
            this.LabelStatus.Text = "Status da TV";
            // 
            // ButtonOnOff
            // 
            this.ButtonOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonOnOff.Location = new System.Drawing.Point(49, 361);
            this.ButtonOnOff.Name = "ButtonOnOff";
            this.ButtonOnOff.Size = new System.Drawing.Size(100, 32);
            this.ButtonOnOff.TabIndex = 1;
            this.ButtonOnOff.Text = "On";
            this.ButtonOnOff.UseVisualStyleBackColor = true;
            this.ButtonOnOff.Click += new System.EventHandler(this.ButtonOnOff_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelCanal);
            this.groupBox1.Controls.Add(this.ButtonAumentaCanal);
            this.groupBox1.Controls.Add(this.ButtonDiminuiCanal);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 79);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Canais";
            // 
            // LabelCanal
            // 
            this.LabelCanal.AutoSize = true;
            this.LabelCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.LabelCanal.Location = new System.Drawing.Point(90, 35);
            this.LabelCanal.Name = "LabelCanal";
            this.LabelCanal.Size = new System.Drawing.Size(22, 16);
            this.LabelCanal.TabIndex = 2;
            this.LabelCanal.Text = "- -";
            // 
            // ButtonAumentaCanal
            // 
            this.ButtonAumentaCanal.Enabled = false;
            this.ButtonAumentaCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAumentaCanal.Location = new System.Drawing.Point(146, 25);
            this.ButtonAumentaCanal.Name = "ButtonAumentaCanal";
            this.ButtonAumentaCanal.Size = new System.Drawing.Size(33, 32);
            this.ButtonAumentaCanal.TabIndex = 1;
            this.ButtonAumentaCanal.Text = "+";
            this.ButtonAumentaCanal.UseVisualStyleBackColor = true;
            this.ButtonAumentaCanal.Click += new System.EventHandler(this.ButtonAumentaCanal_Click);
            // 
            // ButtonDiminuiCanal
            // 
            this.ButtonDiminuiCanal.Enabled = false;
            this.ButtonDiminuiCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDiminuiCanal.Location = new System.Drawing.Point(20, 25);
            this.ButtonDiminuiCanal.Name = "ButtonDiminuiCanal";
            this.ButtonDiminuiCanal.Size = new System.Drawing.Size(32, 32);
            this.ButtonDiminuiCanal.TabIndex = 0;
            this.ButtonDiminuiCanal.Text = "-";
            this.ButtonDiminuiCanal.UseVisualStyleBackColor = true;
            this.ButtonDiminuiCanal.Click += new System.EventHandler(this.ButtonDiminuiCanal_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LabelVolume);
            this.groupBox2.Controls.Add(this.ButtonAumentaVolume);
            this.groupBox2.Controls.Add(this.ButtonDiminuiVolume);
            this.groupBox2.Location = new System.Drawing.Point(13, 99);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 91);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Volume";
            // 
            // LabelVolume
            // 
            this.LabelVolume.AutoSize = true;
            this.LabelVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.LabelVolume.Location = new System.Drawing.Point(93, 39);
            this.LabelVolume.Name = "LabelVolume";
            this.LabelVolume.Size = new System.Drawing.Size(22, 16);
            this.LabelVolume.TabIndex = 5;
            this.LabelVolume.Text = "- -";
            // 
            // ButtonAumentaVolume
            // 
            this.ButtonAumentaVolume.Enabled = false;
            this.ButtonAumentaVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.ButtonAumentaVolume.Location = new System.Drawing.Point(146, 29);
            this.ButtonAumentaVolume.Name = "ButtonAumentaVolume";
            this.ButtonAumentaVolume.Size = new System.Drawing.Size(33, 32);
            this.ButtonAumentaVolume.TabIndex = 4;
            this.ButtonAumentaVolume.Text = "+";
            this.ButtonAumentaVolume.UseVisualStyleBackColor = true;
            this.ButtonAumentaVolume.Click += new System.EventHandler(this.ButtonAumentaVolume_Click);
            // 
            // ButtonDiminuiVolume
            // 
            this.ButtonDiminuiVolume.Enabled = false;
            this.ButtonDiminuiVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.ButtonDiminuiVolume.Location = new System.Drawing.Point(20, 29);
            this.ButtonDiminuiVolume.Name = "ButtonDiminuiVolume";
            this.ButtonDiminuiVolume.Size = new System.Drawing.Size(32, 32);
            this.ButtonDiminuiVolume.TabIndex = 4;
            this.ButtonDiminuiVolume.Text = "-";
            this.ButtonDiminuiVolume.UseVisualStyleBackColor = true;
            this.ButtonDiminuiVolume.Click += new System.EventHandler(this.ButtonDiminuiVolume_Click);
            // 
            // TrackBarVolume1
            // 
            this.TrackBarVolume1.Enabled = false;
            this.TrackBarVolume1.Location = new System.Drawing.Point(20, 31);
            this.TrackBarVolume1.Maximum = 100;
            this.TrackBarVolume1.Name = "TrackBarVolume1";
            this.TrackBarVolume1.Size = new System.Drawing.Size(159, 45);
            this.TrackBarVolume1.TabIndex = 6;
            this.TrackBarVolume1.Scroll += new System.EventHandler(this.TrackBarVolume1_Scroll);
            // 
            // LabelVolume1
            // 
            this.LabelVolume1.AutoSize = true;
            this.LabelVolume1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.LabelVolume1.Location = new System.Drawing.Point(88, 62);
            this.LabelVolume1.Name = "LabelVolume1";
            this.LabelVolume1.Size = new System.Drawing.Size(22, 16);
            this.LabelVolume1.TabIndex = 7;
            this.LabelVolume1.Text = "- -";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.LabelVolume1);
            this.groupBox3.Controls.Add(this.TrackBarVolume1);
            this.groupBox3.Location = new System.Drawing.Point(13, 197);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 100);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Volume 1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 405);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonOnOff);
            this.Controls.Add(this.LabelStatus);
            this.Name = "Form1";
            this.Text = "Televisão";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Button ButtonOnOff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LabelCanal;
        private System.Windows.Forms.Button ButtonAumentaCanal;
        private System.Windows.Forms.Button ButtonDiminuiCanal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label LabelVolume;
        private System.Windows.Forms.Button ButtonAumentaVolume;
        private System.Windows.Forms.Button ButtonDiminuiVolume;
        private System.Windows.Forms.TrackBar TrackBarVolume1;
        private System.Windows.Forms.Label LabelVolume1;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

